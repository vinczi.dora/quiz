const { init } = require('./web/server');
const logger = require('./logger');
const { connect } = require('./db/index');

async function startup() {
    try {
        await init();
        logger.info('server is ready to acceppt connections');
        await connect();
        logger.info('database is ready to acceppt connections');
    } catch (err) {
        logger.error(err);
        process.exit(1);
    }
}

startup();
