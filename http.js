const http = require('http');
const { parse: URLparse } = require('url');
const { parse: queryParse } = require('querystring');

const DEFAULT_PORT = 3000;
let port = DEFAULT_PORT;
const args = process.argv.slice(2);
const logger = require('./logger');

if (args[0]) {
  port = parseInt(args[0], 10);
}

const handler = (req, res) => {
  const reqURL = URLparse(req.url);
  switch (reqURL.pathname) {
    case '/hello': {
      let response = 'hey there';
      const query = queryParse(reqURL.query);
      if (query.nev) {
        response += ` ${query.nev.charAt(0).toUpperCase() + query.nev.slice(1)}`;
      }
      res.end(`${response}!`);
      break;
    }
    default: {
      res.writeHead(404);
      res.end('Not found');
    }
  }
};

const server = http
  .createServer(handler);

server.listen(port);

server.on('error', (err) => {
  logger.error(err);
  process.exit(1);
});
