const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../config');

async function jwtCookieAuth(req, res, next) {
  const token = req.cookies.token;

  if (!token) {
    return res.status(401).send({ message: 'Unauthorized, missing data' });
  }

  try {
    const data = jwt.verify(token, jwtSecret);
    const user = users.findByEmail(data.email);
    // auth middleware - ki melyik endpointhoz ferhet hozza
    // if(user.righs.include('editing')) {
    //     throw new Error('ehhez nincs jogosultságod!')
    // }
  } catch (err) {
    console.log('hiba');
  }

  return next();
}

module.exports = jwtCookieAuth;
