const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const cookieParser = require('cookie-parser');
const http = require('http');
const { promisify } = require('util');
const config = require('../config');
const router = require('./router');
const logger = require('../logger');
const { port } = require('../config');

const app = express();
const server = http.createServer(app);

app.use(cors());
app.use(cookieParser());
app.use(bodyparser.json());

app.use((req, res, next) => {
  logger.debug(`${req.method} ${req.url} at ${new Date()}`);
  next();
});

app.use(router);

app.use((err, req, res, next) => {
  logger.error(err);
  res.status(500);
  return res.send({
    errorCode: 1945,
    errorMessage: 'ez egy hiba',
  });
});

const listenPromise = promisify(server.listen.bind(server, port));
module.exports = {
  init: listenPromise,
};
