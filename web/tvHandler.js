const joi = require('joi');
const tvs = require('../db/tv');

const tvDataSchema = joi.object({
  displaySizeInInches: joi.number().min(1).required(),
  displayType: joi.string().required(),
  resolutionK: joi.number().min(1).required()
}).unknown().required();


async function get(req, res) {
  const tvID = req.params.id;

  const result = await tvs.get(tvID);
  if (!result) {
    res.status(404);
    res.end();
  }

  res.send(result);
}

async function list(req, res) {
  const result = await tvs.list();
  res.send(result);
}

async function insert(req, res) {
  try {
    joi.attempt(req.body, tvDataSchema);
  } catch (err) {
    res.status(400);
    res.send(err.details[0].message);
  }
  const result = await tvs.insert(req.body);
  res.status(201);
  res.send(result);
}

async function update(req, res) {
  try {
    joi.attempt(req.body, tvDataSchema);
  } catch (err) {
    res.status(400);
    res.send(err.details[0].message);
  }
  const tvID = req.params.id;
  const result = await tvs.update(tvID, req.body);
  res.send(result);
}

async function remove(req, res) {
  const tvID = req.params.id;
  await tvs.remove(tvID);
  res.status(204);
}

module.exports = {
  get, list, insert, update, remove,
};
