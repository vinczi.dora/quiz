const jwt = require('jsonwebtoken');
const users = require('../db/user');
const { jwtSecret } = require('../config')

async function register(req, res) {
  const user = await users.register(req.body);
  res.status(201);
  res.send(user);
}

async function login(req, res) {
  const { email, password } = req.body;
  const sessionToken = Buffer
    .from(`${email}:${password}`)
    .toString('base64');
  res.cookie('session', sessionToken, {maxAge: 9000000, httpOnly: true }).end();
}

async function loginJWT(req, res) {
  const { email, password } = req.body;
  // validalni a usert!!! (security probl)
  const token = jwt.sign({ email }, jwtSecret);
  res.send({ token });
}

async function loginJWTCookie(req, res) {
  const { email, password } = req.body;
  // validalni a usert!!! (security probl)
  const token = jwt.sign({ email }, jwtSecret);
  res.cookie('token', token, {maxAge: 9000000000, httpOnly: true}).end();
}


module.exports = {
  register,
  login: loginJWTCookie,
};
