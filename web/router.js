const { Router } = require('express');

const router = Router();
const {
  get, list, insert, update, remove,
} = require('./tvHandler');
const { register, login } = require('./userHandler');
const auth = require('../auth/jwtCookie');

router.get('/', (req, res) => {
  res.send({
    message: 'ok',
  });
});

// tv inventory related endpoints
router.get('/tvs', list);
router.get('/tvs/:id', get);
router.post('/tvs', insert);
router.put('/tvs/:id', update);
router.delete('/tvs/:id', remove);

// user related endpoints
router.post('/register', register);
router.post('/login', login);

module.exports = router;
